const { start, stop, stopAndGetData } = require('audiomodule');
const {app} = require('electron').remote;

window.AudioContext = window.AudioContext || window.webkitAudioContext;
audioContext = new AudioContext();

let startButton = document.getElementById('start');
let stopButton = document.getElementById('stop');
let downloadButton = document.getElementById('download');

startButton.onclick = () => {
  let homeDir = app.getPath('home');
  let filename = homeDir + '/test.wav';
  console.log(filename);
  start(filename);
  console.log('Record started')
};
stopButton.onclick = () => {
  stop();
  console.log('Record stoped')
};
downloadButton.onclick = async () => {
  let wav = await stopAndGetData();
  
  var blob = new window.Blob([ wav ], {
    type: 'audio/wav'
  })
  
  var anchor = document.createElement('a')
  document.body.appendChild(anchor)
  anchor.style = 'display: none'
  var url = window.URL.createObjectURL(blob)
  anchor.href = url
  anchor.download = 'audio.wav'
  anchor.click()
  window.URL.revokeObjectURL(url)

  console.log('Record stoped and downloaded')
};

var convertBlock = (incomingData) => { // incoming data is a UInt8Array
	let view = new DataView(incomingData.buffer);
  var outputData = new Float32Array(incomingData.length/4);
	for (let i = 0; i < incomingData.length/4; i++) {
		outputData[i] = view.getFloat32(i*4, true);
	}
  return outputData;
}
